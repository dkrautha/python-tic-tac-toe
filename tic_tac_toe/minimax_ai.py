import tic_tac_toe.main as main
from tic_tac_toe.basic_ais import get_opponent, get_valid_moves


def minimax_score(board, player):
    # assuming by default we are X
    winner = main.get_winner(board)
    if winner == "X":
        return 10
    elif winner == "O":
        return -10
    elif winner == "Draw":
        return 0

    valid_moves = get_valid_moves(board)
    scores = []

    for move in valid_moves:
        new_board = main.make_move(board, move, player)
        opponent = get_opponent(player)
        score = minimax_score(new_board, opponent)
        scores.append(score)

    if player == "X":
        return max(scores)
    elif player == "O":
        return min(scores)


def minimax_score_with_cache(board, player, cache):
    board_cache_key = str(board)

    if board_cache_key not in cache:
        score = minimax_score(board, player)

        cache[board_cache_key] = score

    return cache[board_cache_key]


def minimax_ai(board, player):
    optimal_score = None
    optimal_move = None

    for move in get_valid_moves(board):
        new_board = main.make_move(board, move, player)
        opponent = get_opponent(player)
        opponent_score = minimax_score(new_board, opponent)

        if player == "X":
            if optimal_score is None or opponent_score > optimal_score:
                optimal_move = move
                optimal_score = opponent_score

        elif player == "O":
            if optimal_score is None or opponent_score < optimal_score:
                optimal_move = move
                optimal_score = opponent_score

    return optimal_move


cache = {}


def minimax_ai_with_cache(board, player):
    optimal_score = None
    optimal_move = None

    for move in get_valid_moves(board):
        new_board = main.make_move(board, move, player)
        opponent = get_opponent(player)
        opponent_score = minimax_score_with_cache(new_board, opponent, cache)

        if player == "X":
            if optimal_score is None or opponent_score > optimal_score:
                optimal_move = move
                optimal_score = opponent_score

        elif player == "O":
            if optimal_score is None or opponent_score < optimal_score:
                optimal_move = move
                optimal_score = opponent_score

    return optimal_move
