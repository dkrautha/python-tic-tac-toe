import random
import tic_tac_toe.main as main

# helper functions


def get_valid_moves(board):
    valid_moves = []
    for y in range(3):
        for x in range(3):
            if main.is_valid_move(board, (x, y)):
                valid_moves += [(x, y)]

    return valid_moves


def get_opponent(player):
    if player == "X":
        return "O"
    elif player == "O":
        return "X"


# ai should always take a board and player as an argument
# and return a valid move at the end: (x, y)


def random_ai(board, player):
    valid_moves = get_valid_moves(board)

    return valid_moves[random.randint(0, len(valid_moves) - 1)]


def finds_winning_moves_ai(board, player):
    valid_moves = get_valid_moves(board)
    winning_move = None
    for move in valid_moves:
        test_board = main.make_move(board, move, player)
        if main.get_winner(test_board) is player:
            winning_move = move
            break

    return winning_move


def finds_winning_and_losing_moves_ai(board, player):
    move = finds_winning_moves_ai(board, player)
    if move != None:
        return move

    other_player = get_opponent(player)
    move = finds_winning_moves_ai(board, other_player)
    if move != None:
        return move

    return random_ai(board, player)


def human_player(board, player):
    return main.get_move()
