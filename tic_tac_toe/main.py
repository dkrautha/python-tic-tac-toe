import os, copy


class FilledSlotError(Exception):
    pass


def new_board():
    return [[None for _ in range(3)] for _ in range(3)]


def render(board):
    count = 0

    print("  0 1 2")
    print("  ------")

    for row in board:
        line = f"{count}|"
        for column in row:
            if column == None:
                line += "  "
            elif column == "X":
                line += "X "
            elif column == "O":
                line += "O "
        line += "|"
        print(line)
        count += 1

    print("  ------")


def get_move():
    x = input("X coord: ")
    y = input("Y coord: ")
    return (x, y)


def make_move(board, coords, who):
    new_board = copy.deepcopy(board)
    x = int(coords[0])
    y = int(coords[1])
    if new_board[y][x] == None:
        new_board[y][x] = who
    else:
        raise FilledSlotError("This slot has already been taken")
    return new_board


def is_valid_move(board, coords):
    try:
        x = int(coords[0])
        y = int(coords[1])
        if board[y][x] == None:
            return True
        else:
            return False
    except IndexError:
        return False
    except TypeError:
        return False
    except ValueError:
        return False


def get_winner(board):
    result = None
    count = 0
    winning_lines = [
        # vertical win cons
        [board[0][0], board[1][0], board[2][0]],
        [board[0][1], board[1][1], board[2][1]],
        [board[0][2], board[1][2], board[2][2]],
        # horizontal win cons
        [board[0][0], board[0][1], board[0][2]],
        [board[1][0], board[1][1], board[1][2]],
        [board[2][0], board[2][1], board[2][2]],
        # diagonal win cons
        [board[0][0], board[1][1], board[2][2]],
        [board[0][2], board[1][1], board[2][0]],
    ]

    for line in winning_lines:
        if line.count(line[0]) == 3 and line[0] != None:
            result = line[0]
        elif line.count(None) == 0:
            count += 1

    if count == 8:
        result = "Draw"

    return result


def main(player1_choice, player2_choice):
    board = new_board()
    player = "X"
    coords = None
    render(board)

    while True:
        winner = get_winner(board)
        if winner != None:
            if winner != "Draw":
                print(f"{winner} wins!")
                break
            else:
                print("Draw!")
                break

        while True:
            print(f"its {player}'s turn")

            if player == "X":
                coords = player1_choice(board, player)

            elif player == "O":
                coords = player2_choice(board, player)

            if is_valid_move(board, coords):
                break
            else:
                print("invalid move, try again")

        board = make_move(board, coords, player)

        if player == "X":
            player = "O"
        elif player == "O":
            player = "X"

        os.system("cls" if os.name == "nt" else "clear")
        render(board)
