from tic_tac_toe import __version__
from tic_tac_toe.main import new_board, make_move, get_winner, main
from tic_tac_toe.basic_ais import (
    random_ai,
    finds_winning_moves_ai,
    finds_winning_and_losing_moves_ai,
    get_valid_moves,
)
import tic_tac_toe.minimax_ai as minimax
import time


def test_version():
    assert __version__ == "0.1.0"


def test_vertical_win():
    board1 = new_board()
    board1 = make_move(board1, (0, 0), "X")
    board1 = make_move(board1, (0, 1), "X")
    board1 = make_move(board1, (0, 2), "X")

    board2 = new_board()
    board2 = make_move(board2, (1, 0), "X")
    board2 = make_move(board2, (1, 1), "X")
    board2 = make_move(board2, (1, 2), "X")

    board3 = new_board()
    board3 = make_move(board3, (2, 0), "X")
    board3 = make_move(board3, (2, 1), "X")
    board3 = make_move(board3, (2, 2), "X")

    expected_outcomes = ["X", "X", "X"]
    actual_outcomes = [get_winner(board1), get_winner(board2), get_winner(board3)]

    assert expected_outcomes == actual_outcomes


def test_horizontal_win():
    board1 = new_board()
    board1 = make_move(board1, (0, 0), "X")
    board1 = make_move(board1, (1, 0), "X")
    board1 = make_move(board1, (2, 0), "X")

    board2 = new_board()
    board2 = make_move(board2, (0, 1), "X")
    board2 = make_move(board2, (1, 1), "X")
    board2 = make_move(board2, (2, 1), "X")

    board3 = new_board()
    board3 = make_move(board3, (0, 1), "X")
    board3 = make_move(board3, (1, 1), "X")
    board3 = make_move(board3, (2, 1), "X")

    expected_outcomes = ["X", "X", "X"]
    actual_outcomes = [get_winner(board1), get_winner(board2), get_winner(board3)]

    assert expected_outcomes == actual_outcomes


def test_diagonal_win():
    board1 = new_board()
    board1 = make_move(board1, (0, 0), "X")
    board1 = make_move(board1, (1, 1), "X")
    board1 = make_move(board1, (2, 2), "X")

    board2 = new_board()
    board2 = make_move(board2, (2, 0), "X")
    board2 = make_move(board2, (1, 1), "X")
    board2 = make_move(board2, (0, 2), "X")

    expected_outcomes = ["X", "X"]
    actual_outcomes = [get_winner(board1), get_winner(board2)]

    assert expected_outcomes == actual_outcomes


def test_draw():
    board1 = new_board()
    board1 = make_move(board1, (0, 0), "X")
    board1 = make_move(board1, (0, 1), "O")
    board1 = make_move(board1, (0, 2), "X")
    board1 = make_move(board1, (1, 1), "O")
    board1 = make_move(board1, (2, 1), "X")
    board1 = make_move(board1, (1, 0), "O")
    board1 = make_move(board1, (1, 2), "X")
    board1 = make_move(board1, (2, 2), "O")
    board1 = make_move(board1, (2, 0), "X")

    expected_outcome = "Draw"
    actual_outcome = get_winner(board1)

    assert expected_outcome == actual_outcome


def test_random_ai():
    board = [["X", "O", None], ["O", "O", None], ["X", None, None]]
    coords = random_ai(board, "X")
    new_board = make_move(board, coords, "X")

    result = False
    for y in range(3):
        for x in range(3):
            print(board[y][x])
            print(new_board[y][x])
            if board[y][x] != new_board[y][x]:
                result = True

    assert result


def test_find_winning_move_ai():
    board = [["X", "O", None], [None, "O", None], ["X", None, None]]

    expected_coords = [(0, 1), (1, 2)]
    actual_coords = [
        finds_winning_moves_ai(board, "X"),
        finds_winning_moves_ai(board, "O"),
    ]

    assert expected_coords == actual_coords


def test_find_winning_and_losing_move_ai():
    board = [["X", "O", None], [None, "O", None], [None, None, None]]

    expected_coords = [(1, 2), (1, 2)]
    actual_coords = [
        finds_winning_and_losing_moves_ai(board, "O"),
        finds_winning_and_losing_moves_ai(board, "X"),
    ]

    assert expected_coords == actual_coords


def test_valid_moves():
    board = [["X", "O", None], [None, "O", None], ["X", None, None]]
    expected_coords = set([(0, 1), (2, 0), (2, 1), (2, 2), (1, 2)])
    actual_coords = set(get_valid_moves(board))
    assert expected_coords == actual_coords


def test_minimax_score():
    board1 = [["X", "O", None], ["X", "O", None], ["X", None, None]]
    board2 = [["X", "O", None], ["X", "O", None], [None, "O", None]]

    expected_scores = [10, -10]
    actual_scores = [
        minimax.minimax_score(board1, "X"),
        minimax.minimax_score(board2, "O"),
    ]

    assert expected_scores == actual_scores


def test_minimax_ai():
    board1 = new_board()
    board1 = make_move(board1, (0, 0), "X")
    board1 = make_move(board1, (0, 2), "O")
    board1 = make_move(board1, (1, 1), "X")

    board2 = [["X", "X", "O"], [None, "O", None], ["X", None, None]]

    board3 = [["X", "O", None], [None, "O", None], [None, None, "X"]]

    board4 = [["X", "O", None], [None, "O", None], ["O", "X", "X"]]

    expected_moves = [(2, 2), (0, 1), (1, 2), (2, 0)]
    actual_moves = [
        minimax.minimax_ai(board1, "O"),
        minimax.minimax_ai(board2, "O"),
        minimax.minimax_ai(board3, "X"),
        minimax.minimax_ai(board4, "X"),
    ]

    assert expected_moves == actual_moves


def test_caching_speed():
    run1_start = time.time()
    main(minimax.minimax_ai_with_cache, minimax.minimax_ai_with_cache)
    run1_end = time.time() - run1_start

    print(run1_end)

    run2_start = time.time()
    main(minimax.minimax_ai_with_cache, minimax.minimax_ai_with_cache)
    run2_end = time.time() - run2_start

    print(run2_end)

    assert run2_end < run1_end
